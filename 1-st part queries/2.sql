select cnt as sessions_per_user, count(*) as cnt
from (
    SELECT fullvisitorid, count(*) as cnt
    FROM `dhh-analytics-hiringspace.GoogleAnalyticseSampl.ga_sessions_export`
    group by fullvisitorid
)
group by sessions_per_user
order by cnt desc
