from pyspark.sql import functions as F

import os
import logging

from config import Config
from ..base import DataLoader

logger = logging.getLogger(__name__)


class GASessionsDataLoader(DataLoader):
    def __init__(self):
        super().__init__()
        self.parquet_data_dir = os.path.join(
            "/", Config.workdir, Config.data_transfer.ga_sessions.local_source
        )

    def execute(self):
        ga_sessions = self.spark.read.parquet(self.parquet_data_dir)
        ga_sessions = self.get_df_with_unique_ids(ga_sessions)
        self.write_to_jdbc(ga_sessions, "ga_sessions")

        hits = (
            ga_sessions.select(
                F.col("id").alias("session_id"), F.explode(F.col("hit")).alias("hits")
            )
            .select(
                F.col("session_id"),
                F.col("hits.hitNumber").alias("hitNumber"),
                F.col("hits.time").alias("time"),
                F.col("hits.hour").alias("hour"),
                F.col("hits.isInteraction").alias("isInteraction"),
                F.col("hits.isEntrance").alias("isEntrance"),
                F.col("hits.isExit").alias("isExit"),
                F.col("hits.type").alias("type"),
                F.col("hits.name").alias("name"),
                F.col("hits.landingScreenName").alias("landingScreenName"),
                F.col("hits.screenName").alias("screenName"),
                F.col("hits.eventCategory").alias("eventCategory"),
                F.col("hits.eventAction").alias("eventAction"),
                F.col("hits.eventLabel").alias("eventLabel"),
                F.col("hits.transactionId").alias("transactionId"),
                F.col("hits.customDimensions").alias("customDimensions"),
            )
            .distinct()
        )
        hits = self.get_df_with_unique_ids(hits)
        self.write_to_jdbc(hits, "hits")

        cd = (
            hits.select(
                F.col("id").alias("hit_id"),
                F.col("session_id"),
                F.col("hitNumber"),
                F.explode(F.col("customDimensions")).alias("cd"),
            )
            .select(
                F.col("hit_id"),
                F.col("session_id"),
                F.col("hitNumber"),
                F.col("cd.index").alias("index"),
                F.col("cd.value").alias("value"),
            )
            .where("index in ('11', '15', '16', '18', '19', '25', '40')")
            .groupBy("session_id", "hit_id", "hitNumber")
            .pivot("index")
            .agg(F.first(F.col("value")))
            .select(
                F.col("hit_id"),
                F.col("session_id"),
                F.col("hitNumber"),
                F.col("11").alias("screenType"),
                F.col("15").alias("locationCountry"),
                F.col("16").alias("locationCity"),
                F.col("18").alias("locationLon"),
                F.col("19").alias("locationLat"),
                F.col("25").alias("orderPaymentMethod"),
                F.col("40").alias("userLoggedIn"),
            )
        ).distinct()
        cd = self.get_df_with_unique_ids(cd)
        self.write_to_jdbc(cd, "cd_pivot")
