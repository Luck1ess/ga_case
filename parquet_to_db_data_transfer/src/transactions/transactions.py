import os
import logging

from config import Config
from ..base import DataLoader

logger = logging.getLogger(__name__)


class TransactionsDataLoader(DataLoader):
    def __init__(self):
        super().__init__()
        self.parquet_data_dir = os.path.join(
            "/", Config.workdir, Config.data_transfer.transactional.local_source
        )

    def execute(self):
        transactional = self.spark.read.parquet(self.parquet_data_dir)
        transactional = self.get_df_with_unique_ids(transactional)
        self.write_to_jdbc(transactional, "transactional")
