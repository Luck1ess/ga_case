import os

from omegaconf import OmegaConf

config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "config.yaml")


class Config:
    _conf = OmegaConf.load(config_path)

    db = _conf.db
    db.jar = os.path.join(os.path.dirname(os.path.abspath(__file__)), db.jar)

    sql_alchemy_url = f"postgresql://{db.user}:{db.password}@{db.host}/{db.db_name}"
    jdbc_url = f"jdbc:postgresql://{db.host}/{db.db_name}"

    data_transfer = _conf.data_transfer

    workdir = os.getenv("WORKDIR", os.path.dirname(os.path.abspath(__file__)))
