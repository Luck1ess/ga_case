insert into cd_pivot (
    "session_id",
    "hit_id",
    "screenType",
    "locationCountry",
    "locationCity",
    "locationLon",
    "locationLat",
    "orderPaymentMethod",
    "userLoggedIn"
)
select *
from crosstab(
            'select session_id,
                 hit_id,
                 customDimensions ->> ''index'' as index,
                 case
                    when (customDimensions ->> ''value'') = ''NA'' then NULL
                    else (customDimensions ->> ''value'')
                 end as value
            from (
                select session_id,
                    id                                      as hit_id,
                    json_array_elements("customDimensions") as customDimensions
                from hits
                where session_id between {min_id} and {max_id}
            ) h
            where (customDimensions ->> ''index'') in (''11'', ''15'', ''16'', ''18'', ''19'', ''25'', ''40'')
            order by 1, 2',
            'select unnest(ARRAY[''11'', ''15'', ''16'', ''18'', ''19'', ''25'', ''40'']) order by 1'
         ) as cd(
                 session_id int,
                 hit_id int,
                 screenType text,
                 locationCountry text,
                 locationCity text,
                 locationLon text,
                 locationLat text,
                 orderPaymentMethod text,
                 userLoggedIn text);
