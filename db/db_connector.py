from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from contextlib import contextmanager
from config import Config


class DBConnector:
    def __init__(self):
        db_url = Config.sql_alchemy_url
        self.engine = create_engine(db_url)
        self.session_maker = sessionmaker(bind=self.engine)

    def get_engine(self):
        return self.engine

    @contextmanager
    def get_session(self, auto_commit=False):
        session = self.session_maker()
        try:
            yield session
            if auto_commit:
                session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()
