with unnest_table as (
        select
            fullvisitorid,
            visitNumber,
            h.time as hit_time,
            h.hitNumber as hit_number,
            cd.value as cd_value
        FROM `dhh-analytics-hiringspace.GoogleAnalyticsSample.ga_sessions_export` t
        left join unnest(t.hit) as h
        left join unnest(customDimensions) as cd
),
    start_time_table as (
        select
            fullvisitorid,
            visitNumber,
            hit_time as start_time
        from unnest_table
        where hit_number = 1
),
    confirm_time_table as (
        select
            fullvisitorid,
            visitNumber,
            hit_time as confirm_time,
            hit_number,
            cd_value,
            rank() over (partition by fullvisitorid, visitNumber order by hit_number) as conformation_num
        from unnest_table
        where cd_value = 'order_confirmation'
    )

 select avg(confirm_time - start_time)
 from start_time_table s
 join confirm_time_table c
    on c.fullvisitorid = s.fullvisitorid
        and c.visitNumber = s.visitNumber
        and c.conformation_num = 1
-- correct answer - 898559.93, I made mistake because didn't think about case when we have more than one conformation
