from pyspark.sql import SparkSession
from pyspark import SparkConf
from pyspark.sql import functions as F

import logging

from config import Config


logger = logging.getLogger(__name__)


class DataLoader:
    def __init__(self):
        conf = (
            SparkConf()
            .setAppName("Transfer data parquet2postgre")
            .setExecutorEnv("PYSPARK_PYTHON", "python3")
            .setExecutorEnv("PYSPARK_DRIVER_PYTHON", "python3")
        )

        self.spark = (
            SparkSession.builder.config(conf=conf)
            .config("spark.jars", Config.db.jar)
            .getOrCreate()
        )

        self.jdbc_properties = {
            "user": Config.db.user,
            "password": Config.db.password,
            "driver": Config.db.jdbc_driver_name,
        }

    def get_table_columns(self, table):
        return self.spark.read.jdbc(
            Config.jdbc_url, table, properties=self.jdbc_properties
        ).columns

    def write_to_jdbc(self, df, table, mode="append"):
        (
            df.select(self.get_table_columns(table))
            .write.mode(mode)
            .jdbc(Config.jdbc_url, table, properties=self.jdbc_properties)
        )
        logger.info(f"Data to {table} has been written")

    @staticmethod
    def get_df_with_unique_ids(df):
        return df.withColumn("id", F.monotonically_increasing_id())
