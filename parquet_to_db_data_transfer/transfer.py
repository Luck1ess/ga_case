from tqdm import tqdm

from parquet_to_db_data_transfer import GASessionsDataLoader, TransactionsDataLoader


if __name__ == "__main__":
    for DataLoader in tqdm([TransactionsDataLoader(), GASessionsDataLoader()]):
        DataLoader.execute()
