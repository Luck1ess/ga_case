FROM python:3

ENV WORKDIR=ga_case
ENV PYTHONPATH=PYTHONPATH:${WORKDIR}

WORKDIR /${WORKDIR}

COPY . .

EXPOSE 80

RUN apt update -y && apt-get install -y software-properties-common && \
    apt-add-repository 'deb http://security.debian.org/debian-security stretch/updates main' && apt update -y && \
    apt-get install -y openjdk-8-jdk-headless && \
    pip install --no-cache-dir -r requirements.txt && \
    export JAVA_HOME && \
    apt-get clean

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
RUN export JAVA_HOME
