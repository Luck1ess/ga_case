from sqlalchemy import Column, Date, Integer, BigInteger, ForeignKey
from sqlalchemy.dialects.postgresql import TEXT

from ..base import Base


class Transactional(Base):
    __tablename__ = "transactional"
    id = Column(BigInteger, primary_key=True)

    orderDate = Column(Date)
    common_name = Column(TEXT)
    deliveryType = Column(TEXT)
    backendOrderId = Column(Integer)
    frontendOrderId = Column(TEXT)
    status_id = Column(Integer)
    declinereason_code = Column(TEXT)
    declinereason_type = Column(TEXT)
    geopointCustomer = Column(TEXT)
    geopointDropoff = Column(TEXT)
