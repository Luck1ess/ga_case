from sqlalchemy import Column, Integer, Boolean, JSON, ARRAY, BigInteger, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import DOUBLE_PRECISION, TEXT

from ..base import Base


class GASessions(Base):
    __tablename__ = "ga_sessions"
    id = Column(BigInteger, primary_key=True)

    fullvisitorid = Column(TEXT)
    visitNumber = Column(BigInteger)
    visitId = Column(BigInteger)
    visitStartTime = Column(BigInteger)
    date = Column(TEXT)
    visits = Column(DOUBLE_PRECISION)
    hits = Column(BigInteger)
    timeOnSite = Column(DOUBLE_PRECISION)
    transactions = Column(DOUBLE_PRECISION)
    transactionRevenue = Column(DOUBLE_PRECISION)
    newVisits = Column(DOUBLE_PRECISION)
    screenviews = Column(DOUBLE_PRECISION)
    uniqueScreenviews = Column(DOUBLE_PRECISION)
    timeOnScreen = Column(DOUBLE_PRECISION)
    totalTransactionRevenue = Column(DOUBLE_PRECISION)
    source = Column(TEXT)
    medium = Column(TEXT)
    browser = Column(TEXT)
    operatingSystem = Column(TEXT)
    isMobile = Column(Boolean)
    deviceCategory = Column(TEXT)
    country = Column(TEXT)


class Hits(Base):
    __tablename__ = "hits"
    id = Column(BigInteger, primary_key=True)
    session_id = Column(BigInteger, ForeignKey('ga_sessions.id', ondelete="CASCADE"), nullable=True)

    hitNumber = Column(Integer)
    time = Column(Integer)
    hour = Column(Integer)
    isInteraction = Column(Boolean)
    isEntrance = Column(Boolean)
    isExit = Column(Boolean)
    type = Column(TEXT)
    name = Column(TEXT)
    landingScreenName = Column(TEXT)
    screenName = Column(TEXT)
    eventCategory = Column(TEXT)
    eventAction = Column(TEXT)
    eventLabel = Column(TEXT)
    transactionId = Column(TEXT)


class CustomDimensionsPivot(Base):
    __tablename__ = "cd_pivot"
    id = Column(BigInteger, primary_key=True)
    session_id = Column(BigInteger, ForeignKey('ga_sessions.id', ondelete="CASCADE"), nullable=True)
    hit_id = Column(BigInteger, ForeignKey('hits.id', ondelete="CASCADE"), nullable=True)
    hitNumber = Column(Integer)

    screenType = Column(TEXT)
    locationCountry = Column(TEXT)
    locationCity = Column(TEXT)
    locationLon = Column(TEXT)
    locationLat = Column(TEXT)
    orderPaymentMethod = Column(TEXT)
    userLoggedIn = Column(TEXT)


GASessions.hits = relationship(
    "Hits",
    cascade="all,delete",
    # back_populates="ga_sessions",
    backref="ga_sessions",
    innerjoin=True,
)

GASessions.cd_pivot = relationship(
    "CustomDimensionsPivot",
    cascade="all,delete",
    backref="ga_sessions",
    # back_populates="ga_sessions",
    innerjoin=True
)

Hits.cd_pivot = relationship(
    "CustomDimensionsPivot",
    cascade="all,delete",
    backref="hits",
    # back_populates="hits",
    innerjoin=True
)
