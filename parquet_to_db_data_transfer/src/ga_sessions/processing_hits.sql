insert into hits (
    "session_id",
    "hitNumber",
    "time",
    "hour",
    "isInteraction",
    "isEntrance",
    "isExit",
    "type",
    "name",
    "landingScreenName",
    "screenName",
    "eventCategory",
    "eventAction",
    "eventLabel",
    "transactionId",
    "customDimensions"
)
select session_id,
       (hits ->> 'hitNumber')::int                     as hitNumber,
       (hits ->> 'time')::int                          as time,
       (hits ->> 'hour')::int                          as hour,
       (hits ->> 'isInteraction')::bool                as isInteraction,
       (hits ->> 'isEntrance')::bool                   as isEntrance,
       (hits ->> 'isExit')::bool                       as isExit,
       hits ->> 'type'                                 as type,
       hits ->> 'name'                                 as name,
       hits ->> 'landingScreenName'                    as landingScreenName,
       hits ->> 'screenName'                           as screenName,
       hits ->> 'eventCategory'                        as eventCategory,
       hits ->> 'eventAction'                          as eventAction,
       hits ->> 'eventLabel'                           as eventLabel,
       hits ->> 'transactionId'                        as transactionId,
       hits -> 'customDimensions'                      as customDimensions
from (
         select id as session_id, unnest(hit) as hits
         from ga_sessions
         where id between {min_id} and {max_id}
     ) ga;
