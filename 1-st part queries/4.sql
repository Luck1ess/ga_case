with pivot_table as (
    select fullvisitorid,
           visitNumber,
           hitNumber,
           transactionId,
           screenType,
           locationCountry,
           locationCity,
           round(cast(locationLon as Float64), 4) as locationLon,
           round(cast(locationLat as Float64), 4) as locationLat,
           orderPaymentMethod,
           userLoggedIn
    from (
             select fullvisitorid,
                    visitNumber,
                    h.hitNumber     as hitNumber,
                    h.transactionId as transactionId,
                    cd.index        as cd_index,
                    cd.value        as cd_value
             FROM ` dhh-analytics-hiringspace.GoogleAnalyticsSample.ga_sessions_export ` t
                 left join unnest(t.hit) as h
                 left join unnest(customDimensions) as cd) pivot
             (
              ANY_VALUE(cd_value)
        for cd_index in (
            11 as screenType,
            15 as locationCountry,
            16 as locationCity,
            18 as locationLon,
            19 as locationLat,
            25 as orderPaymentMethod,
            40 as userLoggedIn
            )
                 )
    where locationLon != 'NA'
      and locationLat != 'NA'
      and locationLon is not null
      and locationLat is not null
      and locationLon != 'null'
      and locationLat != 'null'
      and locationLon != ''
      and locationLat != ''
      and locationLon != 'N/A'
      and locationLat != 'N/A'
),
     ids_with_changed_geo as (
         select distinct st.fullvisitorid
         from (
                  select fullvisitorid,
                         visitNumber,
                         hitNumber,
                         screenType,
                         locationLon,
                         locationLat,
                         rank() over (partition by fullvisitorid, visitNumber order by hitNumber) as rk
                  from pivot_table
                  where screenType in ('shop_list', 'home')
              ) st
                  join (
             select fullvisitorid,
                    visitNumber,
                    hitNumber,
                    screenType,
                    locationLon,
                    locationLat,
                    rank() over (partition by fullvisitorid, visitNumber order by hitNumber) as rk
             from pivot_table
             where screenType in ('checkout', 'order_confirmation')
         ) en
                       on st.fullvisitorid = en.fullvisitorid
                           and st.visitNumber = en.visitNumber
                           and st.rk = 1 and en.rk = 1
                           and (st.locationLon != en.locationLon and st.locationLat != en.locationLat)
     ),
     session_transaction as (
         select p.fullvisitorid,
                t.declinereason_code,
                t.declinereason_type,
                t.geopointCustomer,
                t.geopointDropoff
         from pivot_table p
                  join ids_with_changed_geo using (fullvisitorid)
                  join ` dhh-analytics-hiringspace.BackendDataSample.transactionalData ` t
         on p.transactionId = t.frontendOrderId
     )


select count(*)
from ids_with_changed_geo
-- correct answer - 3691, same reason as in problem 3

select count(distinct fullvisitorid)
from session_transaction
where declinereason_code is not null
-- correct answer - 179, same reason as in problem 3

select count(distinct fullvisitorid)
from session_transaction
where declinereason_code is null and geopointCustomer is not null and geopointDropoff is not null
-- correct answer - 2336, same reason as in problem 3
