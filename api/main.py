from fastapi import FastAPI, HTTPException, status

from db import DBConnector, GASessions, Hits, CustomDimensionsPivot, Transactional

app = FastAPI()


def check_if_adress_changed(data):
    points = set()
    for row in data:
        if (
            row["locationLon"]
            and row["locationLat"]
            and row["locationLon"] != "NA"
            and row["locationLat"] != "NA"
        ):
            lon = (
                row["locationLon"][0:6]
                if "-" not in row["locationLon"]
                else row["locationLon"][0:7]
            )
            lat = (
                row["locationLat"][0:6]
                if "-" not in row["locationLat"]
                else row["locationLat"][0:7]
            )
            points.add((lon, lat))
        if len(points) > 1:
            return True
    return False


def check_if_order_not_canceled(data):
    for row in data:
        if row["declinereason_type"]:
            return False
    return True


def check_if_order_delivered(data):
    for row in data:
        if row["geopointCustomer"] and row["geopointDropoff"]:
            return True
    return False


@app.get("/")
def root():
    return {"message": "Hi, it's my project"}


@app.get("/get_visitor_info/{full_visitor_id}")
def get_user_info(full_visitor_id):
    db_connector = DBConnector()
    with db_connector.get_session() as session:
        data_raw = (
            session.query(
                GASessions.operatingSystem,
                Hits.hitNumber,
                Hits.transactionId,
                Hits.type,
                Hits.isEntrance,
                Hits.isExit,
                Hits.eventCategory,
                CustomDimensionsPivot.screenType,
                CustomDimensionsPivot.locationLon,
                CustomDimensionsPivot.locationLat,
                Transactional.declinereason_type,
                Transactional.geopointCustomer,
                Transactional.geopointDropoff,
            )
            .join(Hits, GASessions.id == Hits.session_id)
            .join(CustomDimensionsPivot, Hits.id == CustomDimensionsPivot.hit_id)
            .join(
                Transactional,
                Transactional.frontendOrderId == Hits.transactionId,
                isouter=True,
            )
            .filter(GASessions.fullvisitorid == full_visitor_id)
            .order_by(Hits.hitNumber)
        ).all()
    data = [row._asdict() for row in data_raw]
    if data:
        address_changed = check_if_adress_changed(data)
        order_placed = check_if_order_not_canceled(data)
        order_delivered = check_if_order_delivered(data) if order_placed else False
        application_type = [
            row["operatingSystem"] for row in data if row["operatingSystem"]
        ][0]
        result = {
            "full_visitor_id": full_visitor_id,
            "address_changed": address_changed,
            "is_order_placed": order_placed,
            "Is_order_delivered": order_delivered,
            "application_type": application_type,
        }
        return result
    else:
        raise HTTPException(
            status_code=status.HTTP_204_NO_CONTENT,
            detail=f"Your visitor id: {full_visitor_id} not found",
        )
